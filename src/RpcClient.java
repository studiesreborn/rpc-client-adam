import Commands.rpc.Base64Command;
import Commands.rpc.MinCommand;
import Commands.rpc.RPCCommand;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 * Created by Adam on 28.03.2016.
 */
public class RpcClient {

    public static void main(String[] args) {
        CommandInterpreter interpreter = new CommandInterpreter();
        interpreter.run();
    }

}
