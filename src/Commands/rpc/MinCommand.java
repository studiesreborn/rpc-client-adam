package Commands.rpc;


import java.util.Scanner;
import java.util.Vector;

/**
 * Created by Adam on 28.03.2016.
 */

public class MinCommand extends RPCCommand{

    public MinCommand(String address) {
        super(address);
    }

    @Override
    protected Vector<Object> params() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wpisz liczbę:");
        Integer a = scanner.nextInt();
        System.out.println("Wpisz liczbę:");
        Integer b = scanner.nextInt();
        Vector<Object> params = new Vector<>();
        params.add(a);
        params.add(b);
        return params;
    }

    @Override
    public String methodName() {
        return "math.min";
    }
}
