package Commands.rpc;

import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

import java.io.IOException;
import java.util.Vector;

public abstract class RPCCommand{
    public String address;

    public RPCCommand(String address) {
        this.address = address;
    }

    public Object run() throws IOException, XmlRpcException {
        XmlRpcClient client = new XmlRpcClient(address);
        return client.execute(methodName(),params());
    }

    protected abstract Vector<Object> params();
    public abstract String methodName();
}
