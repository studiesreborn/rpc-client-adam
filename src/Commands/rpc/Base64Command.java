package Commands.rpc;

import java.util.Scanner;
import java.util.Vector;

/**
 * Created by Adam on 28.03.2016.
 */
public class Base64Command extends RPCCommand {

    public Base64Command(String address) {
        super(address);
    }

    @Override
    protected Vector<Object> params() {
        System.out.println("Wpisz tekst, który chcesz zakodować");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.next();
        Vector<Object> params = new Vector<>();
        params.add(text);
        return params;
    }

    @Override
    public String methodName() {
        return "text.encodeBase64";
    }
}
