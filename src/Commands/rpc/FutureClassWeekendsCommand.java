package Commands.rpc;

import java.util.Vector;

/**
 * Created by Adam on 28.03.2016.
 */
public class FutureClassWeekendsCommand  extends RPCCommand{
    public FutureClassWeekendsCommand(String address) {
        super(address);
    }

    @Override
    protected Vector<Object> params() {
        return new Vector<>();
    }

    @Override
    public String methodName() {
        return "pwr.futureClassWeekends";
    }
}
