package Commands.rpc;

import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.Vector;

/**
 * Created by Adam on 28.03.2016.
 */
public class ClassWeekendBetweenDatesCommand extends RPCCommand {
    public ClassWeekendBetweenDatesCommand(String address) {
        super(address);
    }

    @Override
    protected Vector<Object> params() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wpisz początkową datę w formacie dd.MM.yyyy (np. 20.01.2016.):");
        String first = scanner.next();
        System.out.println("Wpisz końcową datę w formacie dd.MM.yyyy (np. 20.01.2016.):");
        String second = scanner.next();
        Vector<Object> params = new Vector<>();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        try {
            params.add(dateFormatter.parse(first));
            params.add(dateFormatter.parse(second));
        } catch (Exception e) {
            System.out.println("Sorry, cound find date in given input");
        }
        return params;
    }

    @Override
    public String methodName() {
        return "pwr.weekendClassesBetweenDates";
    }
}
