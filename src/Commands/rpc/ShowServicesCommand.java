package Commands.rpc;

import java.util.Vector;

/**
 * Created by Adam on 28.03.2016.
 */
public class ShowServicesCommand extends RPCCommand {
    public ShowServicesCommand(String address) {
        super(address);
    }

    @Override
    public String methodName() {
        return "rpc.showServices";
    }

    @Override
    protected Vector<Object> params() {
        return new Vector<>();
    }
}
