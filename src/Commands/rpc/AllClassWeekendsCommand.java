package Commands.rpc;

import java.util.Vector;

/**
 * Created by Adam on 28.03.2016.
 */
public class AllClassWeekendsCommand extends RPCCommand {

    public AllClassWeekendsCommand(String address) {
        super(address);
    }

    @Override
    protected Vector<Object> params() {
        return new Vector<>();
    }

    @Override
    public String methodName() {
        return "pwr.allClassWeekends";
    }
}
