import Commands.rpc.*;
import org.apache.xmlrpc.XmlRpcException;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Adam on 28.03.2016.
 */
public class CommandInterpreter {

    private List<RPCCommand> commands;
    private String url;

    public void run() {
        loadUrl();
        initializeCommands(url);
        try {
            writeAvailableCommands();
            listenForUserInput();
        } catch (IOException | XmlRpcException e) {
            System.out.println("Sorry, I couldnt find server at given address");
        }
    }

    private void listenForUserInput() {
        while (true) {
            try {
                runCommandFromTerminal();
                writeAvailableCommands();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlRpcException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadUrl() {
        System.out.println("Podaj adres servera wraz z portem. Np: http://localhost:8023");
        Scanner scanner = new Scanner(System.in);
        url = scanner.next();
    }

    private void writeAvailableCommands() throws IOException, XmlRpcException {
        ShowServicesCommand command = new ShowServicesCommand(url);
        System.out.println(command.run());
        System.out.println("\n");
    }

    private void runCommandFromTerminal() throws IOException, XmlRpcException {
        Scanner scanner = new Scanner(System.in);
        RPCCommand command = commandFromText(scanner.next());
        if (command == null) {
            System.out.println("Sorry, such method doesnt exist");
        } else {
            System.out.println("Result = " + command.run() + "\n");
        }
    }

    private void initializeCommands(String url) {
        commands = new LinkedList<>();
        commands.add(new MinCommand(url));
        commands.add(new Base64Command(url));
        commands.add(new ClassWeekendBetweenDatesCommand(url));
        commands.add(new FutureClassWeekendsCommand(url));
        commands.add(new AllClassWeekendsCommand(url));
    }

    public RPCCommand commandFromText(String text) {
        RPCCommand found = null;
        Iterator<RPCCommand> iterator = commands.iterator();
        while (iterator.hasNext() && found == null) {
            RPCCommand next = iterator.next();
            if (next.methodName().equals(text)) {
                found = next;
            }
        }

        return found;
    }
}
